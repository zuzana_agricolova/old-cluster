**Content of repository**

The config files are split per namespace of the old cluster. 

The types of files that we kept are:
*  deployments
*  replication controllers
*  ingresses
*  services
*  secrets

In some namespaces are some of these files missing - it is because they were not existing for corresponding namespace.

---